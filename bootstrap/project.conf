name: freedesktop-sdk-bootstrap

format-version: 12

aliases:
  flathub: https://flathub.org/
  ftp_gnu_org: https://ftp.gnu.org/gnu/

mirrors:
  - name: kernel_org
    aliases:
      ftp_gnu_org:
      - https://mirrors.kernel.org/gnu/

element-path: elements

variables:
  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  guessed-triplet: "$(sh %{sysroot}/usr/share/gnu-config/config.guess)"
  sbindir: "%{bindir}"
  sysconfdir: "%{prefix}/etc"
  localstatedir: "%{prefix}/var"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  sourcedir: "%{debugdir}/source"
  sysroot: /cross-installation
  cross-install: make -j1 install DESTDIR="%{install-root}%{sysroot}"
  tools: /cross
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{target_arch}-unknown-linux-%{abi}"
  gcc_arch: "%{target_arch}"
  abi: "gnu"
  common_flags: "-O2 -g -pipe -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches"
  flags_x86_64: "%{common_flags}"
  flags_i686: "%{common_flags}"
  flags_aarch64: "%{common_flags}"
  flags_arm: "%{common_flags}"
  ldflags_defaults: "-Wl,--as-needed,-z,relro,-z,now"

  (?):
  - target_arch == "i686":
      gcc_arch: "i386"
  - target_arch == "arm":
      abi: "gnueabihf"

  strip-binaries: |
    touch source-files
    touch source-files-sysroot
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
          -print0 | while read -r -d $'\0' file; do
      read -n4 hdr <"${file}" || continue # check for elf header
      if [ "$hdr" != "$(printf \\x7fELF)" ]; then
        continue
      fi
      case "${file}" in
        "%{install-root}%{debugdir}/"*|"%{install-root}%{sysroot}%{debugdir}/"*)
          continue
          ;;
        "%{install-root}%{sysroot}/"*)
          realpath="$(realpath -s --relative-to="%{install-root}%{sysroot}" "${file}")"
          debugfile="%{install-root}%{sysroot}%{debugdir}/${realpath}.debug"
          toolchain="%{tools}/%{triplet}/bin/"
          sourcedir="%{install-root}%{sysroot}%{sourcedir}"
          source_files=source-files-sysroot
          ;;
        *)
          realpath="$(realpath -s --relative-to="%{install-root}" "${file}")"
          debugfile="%{install-root}%{debugdir}/${realpath}.debug"
          sourcedir="%{install-root}%{sourcedir}]"
          source_files=source-files
          ;;
      esac
      if "${toolchain}objdump" -j .gnu_debuglink -s "${file}" &>/dev/null; then
        continue
      fi
      mkdir -p "$(dirname "$debugfile")"
      if [ -x "$(command -v debugedit)" ]; then
        debugedit -i --list-file=${source_files}.part --base-dir="%{build-root}" --dest-dir="%{sourcedir}/%{element-name}" "${file}"
        cat "${source_files}.part" >>"${source_files}"
      fi
      "${toolchain}objcopy" %{objcopy-extract-args} "${file}" "$debugfile"
      chmod 644 "$debugfile"
      mode="$(stat -c 0%a "${file}")"
      [ -w "${file}" ] || chmod +w "${file}"
      "${toolchain}strip" %{strip-args} "${file}"
      "${toolchain}objcopy" %{objcopy-link-args} "$debugfile" "${file}"
      chmod "${mode}" "${file}"
    done
    sort -zu  <source-files | while read -r -d $'\0' source; do
      dst="%{install-root}%{sourcedir}/%{element-name}/${source}"
      src="%{build-root}/${source}"
      if [ -d "${src}" ]; then
        install -m0755 -d "${dst}"
        continue
      fi
      [ -f "${src}" ] || continue
      install -m0644 -D "${src}" "${dst}"
    done
    sort -zu  <source-files-sysroot | while read -r -d $'\0' source; do
      dst="%{install-root}%{sysroot}%{sourcedir}/%{element-name}/${source}"
      src="%{build-root}/${source}"
      if [ -d "${src}" ]; then
        install -m0755 -d "${dst}"
        continue
      fi
      [ -f "${src}" ] || continue
      install -m0644 -D "${src}" "${dst}"
    done

environment:
  FORCE_UNSAFE_CONFIGURE: "1"
  PATH: "%{tools}/bin:/bin"
  (?):
    - target_arch == "x86_64":
        CFLAGS:  "%{flags_x86_64}"
        CXXFLAGS: "%{flags_x86_64}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "i686":
        CFLAGS: "%{flags_i686}"
        CXXFLAGS: "%{flags_i686}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "arm":
        # Nullify '-fexceptions' which breaks build on arm.
        # https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/jobs/110741993
        CFLAGS:  "%{flags_arm} -fno-exceptions"
        CXXFLAGS: "%{flags_arm}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "aarch64":
        CFLAGS:  "%{flags_aarch64}"
        CXXFLAGS: "%{flags_aarch64}"
        LDFLAGS:  "%{ldflags_defaults}"

  # To work-around some issues with Fedora, instead of using ldconfig,
  # libtool reads ld.so.conf to find paths that do not need RPATH.
  # Unfortunately we do not write in ld.so.conf because flatpak
  # expects it empty.
  # We do not use variable %{libdir} or %{prefix} here because elements
  # might redefine those variables. We want LT_SYS_LIBRARY_PATH to have
  # the value that was used for binutils for %{libdir}, so we expand
  # the value.
  LT_SYS_LIBRARY_PATH: "%{sysroot}/usr/lib/%{gcc_triplet}"

split-rules:
  devel:
    - "%{sysroot}%{includedir}"
    - "%{sysroot}%{includedir}/**"
    - "%{sysroot}%{libdir}/pkgconfig"
    - "%{sysroot}%{libdir}/pkgconfig/**"
    - "%{sysroot}%{datadir}/pkgconfig"
    - "%{sysroot}%{datadir}/pkgconfig/**"
    - "%{sysroot}%{datadir}/aclocal"
    - "%{sysroot}%{datadir}/aclocal/**"
    - "%{sysroot}%{prefix}/lib/cmake"
    - "%{sysroot}%{prefix}/lib/cmake/**"
    - "%{sysroot}%{libdir}/cmake"
    - "%{sysroot}%{libdir}/cmake/**"
    - "%{sysroot}%{prefix}/lib/lib*.a"
    - "%{sysroot}%{libdir}/lib*.a"
    - "%{sysroot}%{prefix}/lib/lib*.la"
    - "%{sysroot}%{libdir}/*.la"

  debug:
    - "%{sysroot}%{debugdir}/**"
    - "%{debugdir}/**"

plugins:
  - origin: pip
    package-name: buildstream-external
    sources:
      git_tag: 0

sources:
  git_tag:
    config:
      track-tags: True

options:
  target_arch:
    type: arch
    description: Target architecture
    variable: target_arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

  build_arch:
    type: arch
    description: Build architecture
    variable: build_arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

artifacts:
  url: https://freedesktop-sdk-cache.codethink.co.uk:11001

elements:
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}
